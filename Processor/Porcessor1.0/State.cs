﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Porcessor1._0
{
    public abstract class State
    {
        public abstract State Process();
    }
}
