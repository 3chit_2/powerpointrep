﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Porcessor1._0
{
    class Processor
    {
        public void StartSolve()
        {

        }

        public void SplitExpression(string exp)
        {
            Stack<string> stack = new Stack<string>();
            List<string> tokens;
            tokens = new List<string>(exp.Split(' '));
            int prio;
            string postexp="";

            for (int i = 0; i < tokens.Count; i++)
            {
                string tmp = tokens[i];
                if (tmp == "(" || tmp == ")")
                    tokens.Remove(tmp);
                else
                {
                    if (tmp == "*" || tmp == "/" || tmp == "+" || tmp == "-")
                    {

                        if (stack.Count <= 0)
                            stack.Push(tmp);
                        else
                        {
                            if (stack.Peek() == "*" || stack.Peek() == "/")
                                prio = 1;
                            else
                                prio = 0;

                            if (prio == 1)
                            {
                                if (tmp == "+" || tmp == "-")
                                {
                                    postexp += stack.Pop();
                                    i--;
                                }
                                else
                                {
                                    postexp += stack.Pop();
                                    i--;
                                }
                            }
                            else
                            {
                                if (tmp == "+" || tmp == "-")
                                {
                                    postexp += stack.Pop();
                                    stack.Push(tmp);
                                }
                                else
                                    stack.Push(tmp);
                            }

                        }
                    }
                    else
                        postexp += tmp;
                }
            }
            for (int i = 0; i < stack.Count; i++)
                postexp += stack.Pop();
        }
    }
}





