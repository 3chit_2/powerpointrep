﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Porcessor1._0
{
    public class Blocked : State
    {

        public Blocked()
        {

        }
        public override State Process()
        {
            int timer = 0;
            while (timer < 31)
            {
                System.Threading.Thread.Sleep(1000);
                timer++;
            }
            return new Ready();
        }
    }
}
