﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Porcessor1._0
{
    class Node
    {
                                                                       

        private Node left;
        private Node right;
        private string Value;
        private Stack<string> stack;
        string a, b;
        string[] tokens;
        int count;
        int step;
        //            State state;

        public Node(char op, Node l, Node r)
        {
            left = l;
            right = r;
            Value = op.ToString();
            stack = new Stack<string>();
            tokens = Postfix().Split(' ');
            count = 0;
            step = 0;
        }

        public Node(int count) { this.count = count; }

        public Node(string value)
        {
            left = null;
            right = null;
            Value = value;
            stack = new Stack<string>();
            tokens = Postfix().Split(' ');
            count = 0;
            step = 0;
        }


        public bool Solve(out int result, out string output)
        {
            output = "";
            result = -1;
            for (step = step ; step < tokens.Length ; step++) 
            {
                if (count == 3)
                {
                    count = 0;
                    //             state = new Blocked();
                    return false;
                }
                else
                {
                    switch (tokens[step])
                    {
                        case "+":
                            b = stack.Pop();
                            a = stack.Pop();
                            stack.Push(Convert.ToString(Convert.ToInt32(a) + Convert.ToInt32(b)));
                            output += stack.Peek() + ";";
                            count++;
                            break;
                        case "-":
                            b = stack.Pop();
                            a = stack.Pop();
                            stack.Push(Convert.ToString(Convert.ToInt16(a) - Convert.ToInt16(b)));
                            output += stack.Peek() + ";";
                            count++;
                            break;
                        case "/":
                            b = stack.Pop();
                            a = stack.Pop();
                            stack.Push(Convert.ToString(Convert.ToInt16(a) / Convert.ToInt16(b)));
                            output += stack.Peek() + ";";
                            count++;
                            break;
                        case "*":
                            b = stack.Pop();
                            a = stack.Pop();
                            stack.Push(Convert.ToString(Convert.ToInt16(a) * Convert.ToInt16(b)));
                            output += stack.Peek() + ";";
                            count++;
                            break;
                        default:
                            stack.Push(tokens[step]);
                            break;
                    }
                }
            }
            result = Convert.ToInt32(stack.Pop());
            return true;
        }
        public string Postfix()
        {
            string res = "";
            if (this.left != null)
            {
                res += this.left.Postfix() + " ";
                res += this.right.Postfix() + " ";
            }
            res += this.Value;
            return res;
        }
    }

}

